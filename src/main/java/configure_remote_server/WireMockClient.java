/****************************************************************************
* Copyright 2021 (C) Andrey Tokmakov
* WireMockClient.java class
*
* @name    : WireMockClient.java
* @author  : Tokmakov Andrey
* @version : 1.0
* @since   : Jan 14, 2021
****************************************************************************/

package configure_remote_server;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;

public class WireMockClient {
	/** Connection timeout: **/
	private static final Duration CONNECTION_TIMEOUT = Duration.ofSeconds(10);

	/** REAL TEST LAB SERVER ADDRESS. !!! **/
	/** Test lab MockServer: **/
	// private static final String BASE_URL = "http://127.0.0.1:8085";
	
	private static final int PORT = 8085;
	// private static final String BASE_URL = "ptf01-t03-mck01.lab.nordigy.ru";
	private static final String BASE_URL = "fun01-p01-mck11.lab.nordigy.ru";
	
	
	/** TODO. Add description **/
	private static final String SERVER_ADMIN_URL = BASE_URL + "/__admin";
	
	/** TODO. Add description **/
	private static final String MAPPINGS_URL = SERVER_ADMIN_URL + "/mappings";
	
	/** TODO. Add description **/
	private static final String RESET_STUB_MAPPINGS = MAPPINGS_URL + "/reset";
	
	/** TODO. Add description **/
	private static final String GET_ALL_REQUESTS_URL = SERVER_ADMIN_URL + "/requests";
	
	/** HTTP Client. **/
	private static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .connectTimeout(CONNECTION_TIMEOUT)
            .build();
	
	public void getAllStubs() 
	{
		HttpRequest request = HttpRequest.newBuilder().GET()
						                 .uri(URI.create(MAPPINGS_URL))
						                 .setHeader(HttpHeaders.USER_AGENT, "Java WireMock Client") 
							             .build();	
		HttpResponse<String> response;
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException | InterruptedException exc) {
			System.err.println(exc.getMessage());
			return;
		}

		System.out.println(response.statusCode());
		System.out.println(response.body());
	}
	
    public void addMapping_Test1()
    {
    	// TODO: Add JSON parser builder
        String json = new StringBuilder().append("{")
                .append("\"request\": {")
                .append("\"method\": \"GET\",")
                .append("\"url\": \"/something\"")
                .append("},")
                .append("\"response\": {")
                .append("\"body\": \"Hello world!\",")
                .append("\"headers\": {")
                .append("\"Content-Type\": \"text/plain\"")
                .append("},")
                .append("\"status\": 200")
                .append("}}").toString();
    	
		HttpRequest request = HttpRequest.newBuilder()
						.POST(HttpRequest.BodyPublishers.ofString(json))
		                .uri(URI.create(MAPPINGS_URL))
		                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
		                .setHeader(HttpHeaders.USER_AGENT, "Java WireMock Client") 
		                .build();	
		
        HttpResponse<String> response;
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException | InterruptedException exc) {
			System.err.println(exc.getMessage());
			return;
		}
		
        System.out.println(response.statusCode());
        System.out.println(response.body());
    }
    
    public void addStub(final String jsonStub) {
		HttpRequest request = HttpRequest.newBuilder()
						.POST(HttpRequest.BodyPublishers.ofString(jsonStub))
		                .uri(URI.create(MAPPINGS_URL))
		                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
		                .setHeader(HttpHeaders.USER_AGENT, "Java WireMock Client") 
		                .build();	
		
        HttpResponse<String> response;
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException | InterruptedException exc) {
			System.err.println(exc.getMessage());
			return;
		}
		
        System.out.println(response.statusCode());
        System.out.println(response.body());
    }
    
    public void deleteStub(String stubUuid) {
		HttpRequest request = HttpRequest.newBuilder()
						.POST(HttpRequest.BodyPublishers.noBody())
		                .uri(URI.create(MAPPINGS_URL + stubUuid))
		                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
		                .setHeader(HttpHeaders.USER_AGENT, "Java WireMock Client") 
		                .build();	
		
        HttpResponse<String> response;
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException | InterruptedException exc) {
			System.err.println(exc.getMessage());
			return;
		}
		
        System.out.println(response.statusCode());
        System.out.println(response.body());
    }
    
    public void deleteMappings() {
		HttpRequest request = HttpRequest.newBuilder()
						.DELETE()
		                .uri(URI.create(MAPPINGS_URL))
		                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
		                .setHeader(HttpHeaders.USER_AGENT, "Java WireMock Client") 
		                .build();	
		
        HttpResponse<String> response;
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException | InterruptedException exc) {
			System.err.println(exc.getMessage());
			return;
		}
		
        System.out.println(response.statusCode());
        System.out.println(response.body());
    }
    
    public void resetMappings() {
		HttpRequest request = HttpRequest.newBuilder()
						.POST(HttpRequest.BodyPublishers.noBody())
		                .uri(URI.create(RESET_STUB_MAPPINGS))
		                .setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
		                .setHeader(HttpHeaders.USER_AGENT, "Java WireMock Client") 
		                .build();	
		
        HttpResponse<String> response;
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException | InterruptedException exc) {
			System.err.println(exc.getMessage());
			return;
		}
		
        System.out.println(response.statusCode());
        System.out.println(response.body());
    }
    
    public void getAllRequests() {
		HttpRequest request = HttpRequest.newBuilder()
						.GET()
		                .uri(URI.create(GET_ALL_REQUESTS_URL))
		                .setHeader(HttpHeaders.USER_AGENT, "Java WireMock Client") 
		                .build();	
		
        HttpResponse<String> response;
		try {
			response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException | InterruptedException exc) {
			System.err.println(exc.getMessage());
			return;
		}
		
        System.out.println(response.statusCode());
        System.out.println(response.body());
    }
    
	public void AddStub_Request_Delete() throws IOException {
		WireMock.configureFor(BASE_URL, PORT);
		StubMapping stubMapping  = WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/context"))
				.willReturn(WireMock.aResponse()
				.withHeader(HttpHeaders.CONTENT_TYPE, "text/plain")
				.withStatus(200)
				.withBody("<html><body bgcolor='gray'>1111222223333</body></html>")));
		
		// GET_Request("/context");
		
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			// 
		}
		
		WireMock.removeStub(stubMapping);
	}
	
	public void addMapping2() throws IOException {
		WireMock.configureFor(BASE_URL, PORT);
		StubMapping stubMapping = WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/context"))
				.willReturn(WireMock.aResponse()
				.withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.withStatus(200)
				.withBody("<html><body bgcolor='gray'>Hello!</body></html>")));
	}
	
	public void addMapping_Timeout() throws IOException {
		WireMock.configureFor(BASE_URL, PORT);
		StubMapping stubMapping = WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/timeout"))
				.willReturn(WireMock.aResponse()
				.withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.withStatus(200)
				.withFixedDelay(10000)
				.withBody("<html><body bgcolor='gray'>Hello!</body></html>")));
	}
	
	public void addMapping_Pattern() throws IOException {
		WireMock.configureFor(BASE_URL, PORT);
		StubMapping stubMapping = WireMock.stubFor(WireMock.get(WireMock.urlPathMatching("/restapi/v1.0/internal/account/info"))
				.withQueryParam("accountId", WireMock.equalTo("11223344"))
				.willReturn(WireMock.aResponse()
				.withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
				.withStatus(200)
				.withBody("<html><body bgcolor='gray'>Hello!</body></html>")));
	}
	
	
	
	public void addMapping_Pattern_Templating() throws IOException {
		WireMock.configureFor(BASE_URL, PORT);
		//StubMapping stubMapping = 
		WireMock.stubFor(WireMock.get(WireMock.urlPathMatching("/restapi/v1.0/internal/account/info"))
				//.withQueryParam("accountId", WireMock.equalTo("111"))
				.willReturn(WireMock.aResponse()
				//.withBody("{{request.path.[0]}}")
				.withBody("[{\"account\":\"{{request.query.accountId}}\",\"accountState\\\":\"Confirmed\",\"extensionsCheckResult\":[{\"extension\":\"111222333444555\",\"extensionState\":\"Enabled\"}]}]")
				.withTransformers("response-template")));
	}
	
	public void GET_Request(String url) throws IOException 
	{
        HttpGet request = new HttpGet("http://" + BASE_URL + ":" + PORT  + url);
        request.addHeader(HttpHeaders.USER_AGENT, "ApacheTestClient");

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {
            logResponse(response);
        }
    }
	
	private void logResponse(CloseableHttpResponse response)
			throws ParseException, IOException {
		 System.out.println(response.getStatusLine().toString()); 
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            // return it as a String
            String result = EntityUtils.toString(entity);
            System.out.println(result);
        }
	}
	
    // ****************************************************************** //
    
	public static void main(String[] args) throws IOException {
		WireMockClient client = new WireMockClient();
		
		// client.getAllStubs();

		// client.resetMappings();
		// client.deleteMappings();
		
		// client.addMapping_Test1();
		
		// client.addMapping2();
		// client.addMapping_Timeout();
		// client.AddStub_Request_Delete();
		
		// client.addMapping_Pattern_Templating();
		 
		// client.getAllRequests();
		
		// addStubs(client);
		
		client.GET_Request("/restapi/v1.0/internal/account/info?accountId=11231");
	}
	
	private static void addStubs(WireMockClient client) {
    	// TODO: Add JSON parser builder
        String json = new StringBuilder().append("{")
                .append("\"request\": {")
                .append("\"method\": \"GET\",")
                .append("\"url\": \"/some/thing\"")
                .append("},")
                .append("\"response\": {")
                .append("\"body\": \"Hello world!\",")
                .append("\"headers\": {")
                .append("\"Content-Type\": \"text/plain\"")
                .append("},")
                .append("\"status\": 200")
                .append("}}").toString();
        
        String timeoutStub = new StringBuilder().append("{")
                .append("\"request\": {")
                .append("\"method\": \"GET\",")
                .append("\"url\": \"/stubs/timeout\"")
                .append("},")
                .append("\"response\": {")
                .append("\"body\": \"Hello world!\",\"fixedDelayMilliseconds\": 15000,")
                .append("\"headers\": {")
                .append("\"Content-Type\": \"text/plain\"")
                .append("},")
                .append("\"status\": 200")
                .append("}}").toString();
        
		
		client.addStub(timeoutStub);
	}
}
