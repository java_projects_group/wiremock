/****************************************************************************
* Copyright 2021 (C) Andrey Tokmakov
* WireMockWrapper_Tests.java class
*
* @name    : WireMockWrapper_Tests.java
* @author  : Tokmakov Andrey
* @version : 1.0
* @since   : Mar 10, 2021
****************************************************************************/

package configure_remote_server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHeaders;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import utilities.Utilities;

public class WireMockWrapper_Tests {

	public static void main(String[] args) throws IOException 
	{
		AddStub_Request_Delete();
	}
	
	@SuppressWarnings("static-access")
	public static void AddStub_Request_Delete() throws IOException {
		try (WireMockWrapper wrapper = new WireMockWrapper()) {
			wrapper.stubFor(WireMock.get(WireMock.urlEqualTo("/context"))
					.willReturn(WireMock.aResponse()
					.withHeader(HttpHeaders.CONTENT_TYPE, "text/plain")
					.withStatus(200)
					.withBody("<html><body bgcolor='gray'>11112222233331</body></html>")));
			
			new ApacheWireMockClient().GET_Request("http://ptf01-t03-mck01.lab.nordigy.ru:8085","/context");
			Utilities.sleep(5000);
			
		} catch (final Exception exc) {
			System.err.println(exc);
		}
	}
}


final class WireMockWrapper implements AutoCloseable {
	/** **/
	private final static WireMock wireMock = new WireMock();
	
	/** **/
	private final static List<StubMapping> mappings = new ArrayList<StubMapping>();
	
	static {
		// wireMock.configureFor("127.0.0.1", 8085);
		wireMock.configureFor("ptf01-t03-mck01.lab.nordigy.ru", 8085);
	}
	
	public static StubMapping stubFor(MappingBuilder mappingBuilder) {
		StubMapping mapping = wireMock.givenThat(mappingBuilder);
		/** Check if OK.**/
		mappings.add(mapping);
		return mapping;
	}
	
	public void removeMappings() {
		mappings.forEach(stub -> wireMock.removeStub(stub));
		System.out.println("Deleted");
	}

	@Override
	public void close() throws Exception {
		removeMappings();
	}
}